import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by andriiz on 28.06.17.
 */
public class Main {
    public static void main(String[] args) {
        boolean i = true;
        while (i) {
            Scanner scan = new Scanner(System.in);
            double a, b, result;
            String operator;
            try {
                System.out.println("Enter number a: " );
                a = scan.nextDouble();

                System.out.print("Enter some operator (Please use: /,*,-,+): ");
                operator = scan.next();

                System.out.println("Enter number b: ");
                b = scan.nextDouble();

                result = returnResult(a, b, operator);
                System.out.println("Your result: " + a + operator + b + "=" + result);

                System.out.println("Do you want continue? yes/no");
                String questionContinue = scan.next();
                switch (questionContinue){
                    case "yes": i=i;break;
                    case "no": i=false;break;
                    default:System.out.println("Plese say yes or no");
                }

            } catch (InputMismatchException e) {
                System.out.println("Entered value is not a number. Please enter a number");

            }
        }
    }

    static double returnResult(double a, double b, String operator) {
        double result;
        switch (operator){
            case "+":
                result = a + b;
                return result;
            case "-":
                result = a - b;
                return result;
            case "*":
                result = a * b;
                return result;
            case "/":
                result = a / b;
                return result;
            default:
                System.out.println("ERROR! Operator is wrong");
        }
        return 0;
    }
/*
        static void askContinue(String questionContinue,boolean i){
            switch (questionContinue){
                case "yes": i=i;break;
                case "no": i=false;break;
                default:System.out.println("Plese say yes or no");
            }
        }*/

}


